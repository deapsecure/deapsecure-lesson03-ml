---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

<!-- this is an html comment -->

In this lesson module, we will introduce *machine learning* techniques
which have been applied to address pressing challenges in
the area of cybersecurity.

{% comment %}

Pre-workshop survey link:

[https://odu.co1.qualtrics.com/jfe/form/SV_eFMRd3BU6WA3PPT](https://odu.co1.qualtrics.com/jfe/form/SV_eFMRd3BU6WA3PPT)
![Pre-survey link for workshop 3](fig/PRE-survey-qr-wksp-3.png)

{% endcomment %}

Website:
<https://deapsecure.gitlab.io/deapsecure-lesson03-ml>


## Objectives

Here are our objectives for this module:

<!--
 * Understanding a radio frequency (RF) based approach in monitoring and detecting wireless signals emitted by nearby flying drones.
-->

 * Understanding how machine learning can be employed to detect running applications on a smart phone.

 * Understanding fundamentals of decision tree and support vector machine algorithms for classification.

 * Practical familiarity with classification algorithms.

 * Practical experience with speeding up classification algorithms using parallel computer.

> ## Prerequisites
>
> * **Python programming knowledge** 
>   If you have never learned Python before, we recommend the following lesson:
>   ["Plotting and Programming in Python"](
>       http://swcarpentry.github.io/python-novice-gapminder/
>   ) by Software Carpentry.
>   For other learning resources, please visit [our blog](
>       https://deapsecure.gitlab.io/posts/2020/01/crash-course-python/
>   ).
>
{: .prereq}

{% include links.md %}
