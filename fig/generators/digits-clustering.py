from sklearn import datasets
digits = datasets.load_digits()
# Take the first 500 data points: it's hard to see 1500 points
X = digits.data[:500]
y = digits.target[:500]

from sklearn.manifold import TSNE
tsne = TSNE(n_components=2, random_state=0)

X_2d = tsne.fit_transform(X)
target_ids = range(len(digits.target_names))

from matplotlib import pyplot as plt
fig=plt.figure(figsize=(10, 10))
colors = 'r', 'g', 'b', 'c', 'm', 'y', 'k', 'pink', 'orange', 'purple'
for i, c, label in zip(target_ids, colors, digits.target_names):
    plt.scatter(X_2d[y == i, 0], X_2d[y == i, 1], c=c, label=label)
plt.legend()
plt.xlabel('Dimension 1', fontsize=20)
plt.ylabel('Dimension 2', fontsize=20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
fig.suptitle('Dimensionality Reduction and Clustering')
fig.savefig('Dimensionality Reduction and Clustering_iris data')

# plt.show()

