---
title: Summary
---

Here are the objectives of this training:

    * Understanding a radio frequency (RF) based approach in monitoring and detecting wireless signals on a use-case of drones.

    * Understanding fundamentals of decision tree and support vector machine algorithms for classification.

    * Practical familiarity with classification algorithms.

    * Practical experience with speeding up classification algorithms using parallel computing.workshop survey link:

{% comment %}

[https://odu.co1.qualtrics.com/jfe/form/SV_djaSReVAPVhLbi5](https://odu.co1.qualtrics.com/jfe/form/SV_djaSReVAPVhLbi5)
![POST-survey-qr-wksp-4.png](../fig/POST-survey-qr-wksp-4.png)

{% endcomment %}


{% comment %}
{% include carpentries.html %}
{% endcomment %}
{% include links.md %}
