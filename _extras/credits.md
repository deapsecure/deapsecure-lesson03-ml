---
title: "Credits for materials used"
---

## Image credits

* Linear regression, by Wikipedia user `Sewaqu`

  https://en.wikipedia.org/wiki/Regression_analysis#/media/File:Linear_regression.svg

* Semi-supervised leaning, by Wikipedia user `Techerin`

  https://upload.wikimedia.org/wikipedia/commons/d/d0/Example_of_unlabeled_data_in_semisupervised_learning.png

* Reinforcement leaning, by Wikipedia user `Megajuice`
  https://commons.wikimedia.org/wiki/File:Reinforcement_learning_diagram.svg#/media/File:Reinforcement_learning_diagram.svg

* Precision and recall, by Wikipedia user `Walber`

  https://en.wikipedia.org/wiki/Precision_and_recall#/media/File:Precisionrecall.svg

* Confusion matrix, from Wikipedia article

  https://en.wikipedia.org/wiki/Confusion_matrix

* SVC illustration for "iris" dataset, from Scikit-Learn's user's guide

  https://scikit-learn.org/stable/modules/svm.html#svm-classification


## Research: Drone Detection & Classification

Bello, Abdulkabir.
"Radio Frequency Toolbox for Drone Detection and Classification" (2019).
Master of Science (MS), Thesis, Electrical/Computer Engineering,
Old Dominion University.
Thesis Adviser: Sachin Shetty.
DOI: 10.25777/9gkm-jd54 . <br>
<https://digitalcommons.odu.edu/ece_etds/160>



{% include links.md %}
