---
title: "An Introduction to Scikit-Learn and Pandas"
teaching: 0
exercises: 0
questions:
- "What is Scikit-Learn?"
- "What is Pandas?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "Scikit-Learn provides machine learning capabilities for Python."
- "Pandas provides data handling and analytic tools for Python."
---

## About Scikit-Learn

Scikit-Learn is an open-source toolkit for machine learning in Python.
It is built upon well-established Python numerical and scientific computation
toolboxes: NumPy, SciPy, as well as Matplotlib.

Scikit-learn contains many supervised machine learning methods
(both classification and regression methods),
as well as unsupervised methods (clustering).
In addition, there are also additional algorithms to
reduce the dimensionality of the problem (e.g. the principal component analysis [PCA]),
and many convenience tools to prepare the data,
measure the accuracy of the trained ML algorithm.

![Scikit-learn front web page](../fig/Scikit-learn-website1.png)

The project website is [https://scikit-learn.org/](https://scikit-learn.org/).
The website is full of helpful tutorials, documentation, as well as technical
instructions.
For every method, there is a technical explanation about the method,
including pointers about the advantage and disadvantages of the ML method;
but there is also detailed programming documentation
(the API [Application Programming Interface]).
Each documentation is accompanied by plenty of examples.

As an example, consider the support vector machines (SVM) method:

* [This website](https://scikit-learn.org/stable/modules/svm.html)
  provides the technical overview of SVM;

* [The generated API documentation](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html#sklearn.svm.SVC)
  provides the exact details of the `SVC` class.

* [Further down the API document](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html#examples-using-sklearn-svm-svc),
  one will find many worked examples on the use of SVM method.

While learning about ML techniques in this workshop, it is highly recommended
that you also consult Scikit-Learn's website for further information.
It is an exceptional resource for learning and reference purposes.


## About Pandas

Pandas is a data analysis library for Python.
While Scikit-Learn provides the machine-learning capabilities to Python,
Pandas provides powerful and convenience data-handling capabilities that
complements Scikit-Learn, NumPy, etc.
Pandas is using NumPy arrays under the hood, but it provides
powerful data analytics capabilities, which are not part of NumPy capabilities.
One can perform filtering, data transformation, joining of two or more
DataFrames, etc. using Pandas;
these steps are often necessary in preparation of machine learning.


<!---
### Comparing Pandas vs. Spark

Pandas can be compared to Spark in many ways: Both offer `DataFrame`,
an object that embodies a dataset in a tabular format.
Here are some similarities:

* Both Spark and Pandas have capabilities to read from and write from
  popular common data formats:

  * [Pandas IO tools](https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html)
    can read/write CSV, JSON, Excel, HDF5, and even Apache's Parquet tabular format.
    Database formats can be supported via SQL queries.

  * [Spark DataFrameReader](
        https://jaceklaskowski.gitbooks.io/mastering-spark-sql/spark-sql-DataFrameReader.html
    ), which is customarily accessed via `spark.read` prefix,
    also has a rich support for formats like CSV, JSON, database tables
    (via JDBC), Apache Parquet, etc.
    See also [the web page on Data Sources on Spark's User Guide](
        https://spark.apache.org/docs/latest/sql-data-sources.html
    ).

But there are several important differences:

* In Pandas, a `DataFrame` stores the dataset "in-memory"; therefore
  the computer must have sufficient memory to contain the entire dataset.
  Furthermore, the dataset can only reside in a single computer;
  it is not distributed.

* In Spark, a `DataFrame` represents a (potentially) enormous amount of data
  that can reside in a distributed fashion (across many computers).
  It is not a requirement that the dataset must fit in the computer's memory,
  because Spark is capable of ingesting and processing data that is larger than
  the aggregated memory of the computers in which the Spark subtasks are executed.

In terms of data operation, there is also an important difference:

* In Pandas, any operation (select, filter, join, etc.) will be
  executed immediately and returns a new DataFrame with the computed results.
  In other words, the results are available immediately following the command.

* In Spark, only *actions* will return results that can be fetch, saved, or
  visualized.
  The other type of operations (*transformations*) will only build
  the task pipeline without executing it right away.

-->

In this lesson, we choose to use Pandas because it works well in conjunction
with Scikit-Learn.
(Pandas also work well with popular deep learning frameworks such as KERAS.)
At the end of this lesson, we will briefly present an alternative to perform
ML workflow on Spark, using a library called "MLlib".


{% include links.md %}
