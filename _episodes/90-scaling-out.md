---
title: "Scaling Out: Extreme-scale Machine Learning"
teaching: 0
exercises: 0
questions:
- "How can I perform machine learning on extremely large datasets?"
- "What can be accomplished by computers through machine learning?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "Spark MLlib provides a way to apply machine learning algorithms on extremely large datasets."
---

There are several ways to go about to perform machine learning at extreme scale
(for example, with very large datasets, ...).

One way is to leverage Spark and its machine learning library (MLlib).



{% include links.md %}
