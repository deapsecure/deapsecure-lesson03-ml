---
title: About
---

This lesson is part of DeapSECURE cyberinfrastructure training program
at the Old Dominion University.
The program is funded by National Science Foundation through CyberTraining
grant #1829771.

This website was made using The Carpentries' lesson template,
https://github.com/carpentries/styles .

{% comment %}
{% include carpentries.html %}
{% endcomment %}
{% include links.md %}
